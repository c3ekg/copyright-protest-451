A protest against added points to EU copyright directive
===========
Features an error-page for http-status code "Error 451 - Unavilable for legal reasons."
Setup steps for systems running nginx included.

View the demo [here](https://kindergarten.chaospott.de/eu_copyright)
