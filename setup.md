Installation instructions
=======
**Clone this repository onto your device:**
```bash
cd /opt
git clone https://gitlab.com/c3ekg/copyright-protest-451.git
```

**Link webroot-folder into your webroot**
```bash
ln -s /opt/copyright-protest-451/webroot/ /var/www/html/451
```
If you changed your webroot, you need to replace /var/www/html/ with it.

**Setup nginx to throw error 451 and show this page**
```bash
cd /etc/nginx/sites-available
```
Than edit default with your preferred text editor. You may need to vary this, if you have multiple domains on this server.
Add the following lines after the line starting with "root". Make sure, you are a line above the "}"
```bash
        error_page 451 /451/index.html;
        location /eu_copyright {
                return 451;
        }

```
You need to modify this step, if you use other webservers like apache2
You can replace eu_copyright by the url, you want it on.

Now open your browser and navigate to <your-page>/eu_copyright (or whatever you replaced it with). You should see a protest page
To test the http-status run
```bash
curl -I <your-page>/eu_copyright (or whatever you replaced it with)
```
You should see "451" near http** (first line of output)

Have fun.
